#pragma once
#include "SDLLib.h"
#include "XPLMDataAccess.h"
#include "LinearFunction.h"

using namespace std;

namespace Joystick {
	class CAxis
	{
		Uint8 m_id;
		Sint16 m_old_value;
		Sint16 m_new_value;

		float m_old_f_value;
		float m_new_f_value;

		XPLMDataRef dataref;

		CFunction *m_function;

	public:
		CAxis(Uint8 id, const char *inDataRefName, unsigned char functionType, void *pParams);
		~CAxis();

		Uint8 GetId();
		void SetState(Sint16 value);
		void CAxis::Translate();
	};
}

