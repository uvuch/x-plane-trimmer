#pragma once
#include "Function.h"

namespace Joystick {
	typedef struct LinearParams{
		double minimum;
		double maximum;
		bool invert;
		LinearParams(double min, double max, bool invert = false) : minimum(min), maximum(max), invert(invert) {}
	} t_LinearParams;

	class CLinearFunction : public CFunction
	{
		t_LinearParams *m_pParams;

	public:
		CLinearFunction(t_LinearParams *pParams);
		~CLinearFunction();

		double Calculate(signed int);
	};
}

