#include "Console.h"

CConsole *CConsole::m_pInstance = nullptr;
const float CConsole::COLOR_WHITE[] = { 1.0, 1.0, 1.0 }; /* RGB White */

CConsole::CConsole() {
	int width, height;
	XPLMGetScreenSize(&width, &height);

	this->left = 50;
	this->top = height - 90;
	this->right = 540;
	this->bottom = this->top - 415;
	this->gWindow = NULL;
	this->mpBuffer.reserve(CConsole::BUFFER_LENGTH);
	for (int i = 0; i < CConsole::BUFFER_LENGTH; i++)
		this->mpBuffer.push_back((char *) "");
}

CConsole::~CConsole() {
	this->hide();
	this->mpBuffer.clear();
}

CConsole* CConsole::Instance() {
	if (CConsole::m_pInstance == nullptr) CConsole::m_pInstance = new CConsole();
	return CConsole::m_pInstance;
}

void CConsole::Close() {
	if (CConsole::m_pInstance != nullptr) {
		delete CConsole::m_pInstance;
		CConsole::m_pInstance = nullptr;
	}
}

bool CConsole::IsOpen() {
	if (CConsole::m_pInstance == nullptr) return false;
	return CConsole::m_pInstance->gWindow != nullptr;
}

void CConsole::show() {
	if (this->gWindow != NULL) return;

	this->gWindow = XPLMCreateWindow(
		this->left, this->top, this->right, this->bottom, 1,	/* Area of the window. Start visible */
		CConsole::WindowDrawCallback, CConsole::WindowKeyHandler, CConsole::WindowMouseHandler,	/* Callbacks */
		this	// Pass console instance as hRef to be passed to callbacks later
		);
}

void CConsole::hide(){
	if (this->gWindow) {
		XPLMDestroyWindow(this->gWindow);
		this->gWindow = NULL;
	}
}

void CConsole::print(const char *s) {
	this->mpBuffer.erase(this->mpBuffer.begin());
	this->mpBuffer.push_back((string)s);
}

void CConsole::WindowDrawCallback(XPLMWindowID inWindowID, void *inRefcon) {
	CConsole *console = (CConsole *)inRefcon;

	XPLMDrawTranslucentDarkBox(console->left, console->top, console->right, console->bottom);

	for (int i = 0; i < CConsole::BUFFER_LENGTH; i++) {
		XPLMDrawString((float*)CConsole::COLOR_WHITE,
			console->left + 5,
			console->top - 15 - 10 * i,
			(char*)console->mpBuffer[i].c_str(),
			NULL,
			xplmFont_Basic);
	}
}

int CConsole::WindowMouseHandler(XPLMWindowID inWindowID, int x, int y, XPLMMouseStatus inMouse, void *inRefcon) { return 0; }
void CConsole::WindowKeyHandler(XPLMWindowID inWindowID, char inKey, XPLMKeyFlags inFlags, char inVirtualKey, void *inRefcon, int losingFocus) {}