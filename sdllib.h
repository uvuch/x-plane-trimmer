#pragma once
#include "config.h"
#include "SDL.h"

class CSDLLib
{
	static CSDLLib *m_pInstance;
	static bool m_bInitialized;
	static Uint64 m_iFlags;

	CSDLLib(Uint64 m_iFlags);
	~CSDLLib();

public:
	static CSDLLib* Instance();
	static CSDLLib* Instance(Uint64 m_iFlags);
	static void Close();
	static bool Initialized();
};
