#include "config.h"
#include "Console.h"
#include "XPLMPlugin.h"
#include "XPLMMenus.h"

class CMenu {
	static CMenu *m_pMenu;
	XPLMMenuID	m_MenuId;
	int	m_miToggleConsole;
	bool m_bConsoleOpen;

	CMenu();
	~CMenu();

public:
	static CMenu* Instance();
	static void Remove();
	static void Handle(void * mRef, void * iRef);
	bool ToggleConsole();
};
