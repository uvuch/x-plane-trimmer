#include "LinearFunction.h"

using namespace Joystick;

CLinearFunction::CLinearFunction(t_LinearParams *pParams) : m_pParams(pParams) {}
CLinearFunction::~CLinearFunction() { delete this->m_pParams; }

double CLinearFunction::Calculate(signed int x) {
	double res = (this->m_pParams->maximum - this->m_pParams->minimum) / 65535 * (x + 32768);
	if (this->m_pParams->invert) res = this->m_pParams->maximum - res;

	return res;
}