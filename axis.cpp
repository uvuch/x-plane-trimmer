#include "Axis.h"
using namespace Joystick;

CAxis::CAxis(Uint8 id, const char *inDataRefName, unsigned char functionType, void *pParams) : m_id(id) {
	this->dataref = XPLMFindDataRef(inDataRefName);
	switch (functionType) {
	case CFunction::LINEAR:
		this->m_function = (CFunction*) new CLinearFunction((t_LinearParams*)pParams);
		break;
	}

}

CAxis::~CAxis() {
	delete this->m_function;
}

void CAxis::SetState(Sint16 value) {
	this->m_new_value = value;
}

void CAxis::Translate() {
	if (this->m_new_value == this->m_old_value) return;

	this->m_old_value = this->m_new_value;
	this->m_new_f_value = (float) this->m_function->Calculate(this->m_new_value);

	if (this->m_new_f_value != this->m_old_f_value && this->dataref) {
		XPLMSetDataf(this->dataref, this->m_new_f_value);
		this->m_old_f_value = this->m_new_f_value;
	}
}

Uint8 CAxis::GetId() {
	return this->m_id;
}