#include "main.h"

#if IBM
BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#endif



/*
* XPluginStart
* Our start routine registers our window and does any other initialization we must do.
*/
PLUGIN_API int XPluginStart(char *outName, char *outSig, char *outDesc) {
	strcpy(outName, "Trimmer");
	strcpy(outSig, "uvuch.sdl.trimmer");
	strcpy(outDesc, "A plugin that allows to make friends stabilizer trimmer axes with autopilot.");

	CConsole::Instance()->print("> Consoled started");
	CMenu::Instance();
	CSDLLib::Instance(SDL_INIT_JOYSTICK | SDL_INIT_EVENTS);
	if (!CSDLLib::Initialized()) return 0;

	XPLMRegisterFlightLoopCallback(FlightLoop, -1, NULL);

	// We must return 1 to indicate successful initialization, otherwise we will not be called back again.
	return 1;
}

/*
* XPluginStop
* Our cleanup routine deallocates our window.
*/
PLUGIN_API void	XPluginStop(void) {
	XPLMUnregisterFlightLoopCallback(FlightLoop, NULL);
	CSDLLib::Close();
	CMenu::Remove();
	CConsole::Close();
}

/*
* XPluginDisable
* We do not need to do anything when we are disabled, but we must provide the handler.
*/
PLUGIN_API void XPluginDisable(void) {
	if (CConsole::IsOpen()) CMenu::Instance()->ToggleConsole();
	CConsole::Close();
}

/*
* XPluginEnable.
* We don't do any enable-specific initialization, but we must return 1 to indicate that we may be enabled at this time.
*/
PLUGIN_API int XPluginEnable(void) {
	CConsole::Instance();
	return 1;
}

/*
* XPluginReceiveMessage
* We don't have to do anything in our receive message handler, but we must provide one.
*/
PLUGIN_API void XPluginReceiveMessage(XPLMPluginID	inFromWho, int	inMessage, void *inParam) {}


float FlightLoop(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void *inRefcon) {
	SDL_Event event;
	CJoystick *pJoystick;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_JOYDEVICEADDED:
			pJoystick = new CJoystick(event.jdevice.which);
			//if (!(pJoystick->IsOpen() && CJoysticksList::Instance()->Attach(pJoystick)))
			//	delete pJoystick;
			break;

		case SDL_JOYDEVICEREMOVED:
			/* Method detach inside tries to find the item first. So no need for additional scan here. */
			//CJoysticksList::Instance()->Detach(event.jdevice.which);
			break;

		case SDL_JOYAXISMOTION:
			//pJoystick = CJoysticksList::Instance()->Get(event.jaxis.which);
			if (pJoystick != nullptr)
				pJoystick->SetAxisState(event.jaxis.axis, event.jaxis.value);
			break;

		case SDL_JOYHATMOTION:
			break;
		case SDL_JOYBUTTONDOWN:
			break;
		case SDL_JOYBUTTONUP:
			break;
		}
	}

	//CJoysticksList::Instance()->TranslateAll();
	return -1;
}