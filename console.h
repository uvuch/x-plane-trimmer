#ifndef _console_6292016_
#define _console_6292016_

#include "config.h"

#include <string>
#include <vector>

#include "XPLMPlugin.h"
#pragma once

#include "XPLMDisplay.h"
#include "XPLMGraphics.h"

using namespace std;

class CConsole {
	static CConsole *m_pInstance;
	static const int BUFFER_LENGTH = 40;
	static const float COLOR_WHITE[];
	int	left, top, right, bottom;

	vector<string> mpBuffer;
	XPLMWindowID gWindow;

	CConsole();
	~CConsole();

public:
	static CConsole* Instance();
	static void Close();
	static bool IsOpen();

	static void WindowDrawCallback(XPLMWindowID inWindowID, void *inRefcon);
	static int WindowMouseHandler(XPLMWindowID inWindowID, int x, int y, XPLMMouseStatus inMouse, void *inRefcon);
	static void WindowKeyHandler(XPLMWindowID inWindowID, char inKey, XPLMKeyFlags inFlags, char inVirtualKey, void *inRefcon, int losingFocus);

	void show();
	void hide();
	void print(const char *s);
};

#endif