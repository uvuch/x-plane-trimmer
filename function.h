#pragma once

namespace Joystick {
	class CFunction
	{
	public:
		static const unsigned char LINEAR = 1;
		static const unsigned char STEPS = 2;

		virtual double Calculate(signed int) = 0;
	};
}