#include "SDLLib.h"

CSDLLib *CSDLLib::m_pInstance = nullptr;
bool CSDLLib::m_bInitialized = false;
Uint64 CSDLLib::m_iFlags = SDL_INIT_EVERYTHING;

CSDLLib::CSDLLib(Uint64 m_iFlags)
{
	SDL_Init(m_iFlags);
	if (SDL_WasInit(SDL_INIT_EVENTS)) SDL_JoystickEventState(SDL_ENABLE);
	CSDLLib::m_bInitialized = SDL_WasInit(m_iFlags) == m_iFlags;
}


CSDLLib::~CSDLLib()
{
	if (SDL_WasInit(SDL_INIT_EVENTS) &&
		SDL_JoystickEventState(SDL_QUERY) == SDL_ENABLE) SDL_JoystickEventState(SDL_IGNORE);

	// No need to check if sdl was init when we wah=nt to quit. It checks itself inside.
	SDL_Quit();
}

CSDLLib* CSDLLib::Instance() {
	if (CSDLLib::m_pInstance == nullptr) {
		CSDLLib::m_pInstance = new CSDLLib(CSDLLib::m_iFlags);
		if (!CSDLLib::m_bInitialized) CSDLLib::Close();
	}

	return CSDLLib::m_pInstance;
}

CSDLLib* CSDLLib::Instance(Uint64 flags) {
	CSDLLib::m_iFlags = flags;
	return CSDLLib::Instance();
}

void CSDLLib::Close() {
	if (CSDLLib::m_pInstance != nullptr) {
		delete CSDLLib::m_pInstance;
		CSDLLib::m_pInstance = nullptr;
	}
}

bool CSDLLib::Initialized() {
	return CSDLLib::m_pInstance != nullptr;
}