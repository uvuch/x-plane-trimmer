#include "Joystick.h"

CJoystick::CJoystick(int joystick_index)
{
	this->m_pSdlJoystick = SDL_JoystickOpen(joystick_index);
	if (this->m_pSdlJoystick)
		this->m_InstanceId = SDL_JoystickInstanceID(this->m_pSdlJoystick);
}

CJoystick::~CJoystick()
{
	if (this->m_pSdlJoystick) SDL_JoystickClose(this->m_pSdlJoystick);
	this->m_pSdlJoystick = nullptr;
}

bool CJoystick::IsOpen() {
	return this->m_pSdlJoystick != nullptr;
}

SDL_JoystickID CJoystick::InstanceId() {
	return this->m_InstanceId;
}

SDL_JoystickGUID CJoystick::GetGUID() {
	return SDL_JoystickGetGUID(this->m_pSdlJoystick);
}

void CJoystick::AddAxis(CAxis* pAxis) {
	pair<map<Uint8, CAxis*>::iterator, bool> ret;
	this->m_axes.insert(make_pair(pAxis->GetId(), pAxis));

	Sint16 state = SDL_JoystickGetAxis(this->m_pSdlJoystick, pAxis->GetId());
	pAxis->SetState(state);
}

void CJoystick::SetAxisState(Uint8 axis_index, Sint16 state) {
	map<Uint8, CAxis*>::iterator itr;

	itr = this->m_axes.find(axis_index);
	if (itr != this->m_axes.end()) {
		CAxis * axis = (CAxis*)itr->second;
		axis->SetState(state);
	}
}

void CJoystick::TranslateAxes() {
	map<Uint8, CAxis*>::iterator itr;
	for (itr = this->m_axes.begin(); itr != this->m_axes.end(); ++itr)
		itr->second->Translate();
}
