#include "config.h"

#if IBM
#include <windows.h>
#endif

#include "XPLMPlugin.h"
#include "XPLMProcessing.h"
#include "Menu.h"
#include "SDLLib.h"
#include "Joystick.h"

float FlightLoop(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void *inRefcon);
