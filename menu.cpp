#include "Menu.h"

CMenu *CMenu::m_pMenu = nullptr;

CMenu::CMenu() {
	this->m_bConsoleOpen = false;
	int plugins_menu = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "Joystick", NULL, 1);
	this->m_MenuId = XPLMCreateMenu("Joystick", XPLMFindPluginsMenu(), plugins_menu, CMenu::Handle, NULL);
	this->m_miToggleConsole = XPLMAppendMenuItem(this->m_MenuId, "Open Console", (void *)"toggle_console", NULL);
}

CMenu::~CMenu() {
	XPLMDestroyMenu(this->m_MenuId);
}

CMenu* CMenu::Instance() {
	if (CMenu::m_pMenu == nullptr) CMenu::m_pMenu = new CMenu();
	return CMenu::m_pMenu;
}

void CMenu::Remove() {
	if (CMenu::m_pMenu) {
		delete CMenu::m_pMenu;
		CMenu::m_pMenu = nullptr;
	}
}

bool CMenu::ToggleConsole() {
	this->m_bConsoleOpen = !this->m_bConsoleOpen;

	if (this->m_bConsoleOpen) {
		XPLMSetMenuItemName(this->m_MenuId, this->m_miToggleConsole, (const char *) "Close Console", 1);
		CConsole::Instance()->show();
	}
	else {
		XPLMSetMenuItemName(this->m_MenuId, this->m_miToggleConsole, (const char *) "Open Console", 1);
		CConsole::Instance()->hide();
	}

	return this->m_bConsoleOpen;
}

void CMenu::Handle(void * mRef, void * iRef) {
	if (!strcmp((char *)iRef, "toggle_console"))
		CMenu::Instance()->ToggleConsole();
}
