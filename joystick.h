#pragma once

#include "SDLLib.h"
#include <map>
#include "Axis.h"


using namespace Joystick;

class CJoystick
{
	SDL_Joystick *m_pSdlJoystick;
	SDL_JoystickID m_InstanceId;

	map<Uint8, CAxis*> m_axes;

public:
	CJoystick(int joystick_index);
	~CJoystick();

	bool IsOpen();
	SDL_JoystickID InstanceId();
	SDL_JoystickGUID GetGUID();

	void AddAxis(CAxis* pAxis);

	void SetAxisState(Uint8 axis_index, Sint16 state);

	void TranslateAxes();
};

